﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_LINQ
{
    class History
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime OperationTime { get; set; }
        public OperationType OperationType { get; set; }

        public decimal Sum { get; set; }
        public int AccountId { get; set; }
    }

    enum OperationType
    {
        Debit,
        Credit
    }
}
