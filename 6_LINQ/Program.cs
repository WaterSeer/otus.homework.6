﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _6_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {            
            SeedDataBase();            
            Console.WriteLine(InfoByLogndAndPassword("2222", "2222"));
            
            foreach (var item in GetAllAccountsByUser(0))
            {
                Console.WriteLine(item.ToString());
            }
            
            GetAllAccountsHistoryByUser(0);
            
            GetAllCreditOperationsWithAccountMark();
            
            GetUserWhereDebitMoreX(40000);
            Console.ReadLine();
        }

        //1.Вывод информации о заданном аккаунте по логину и паролю
        public static string InfoByLogndAndPassword(string login, string pass)
        {
            using (AppContext db = new AppContext())
            {
                var userSeek = (from user in db.Users
                                where string.Compare(user.Login,login)==0 && string.Compare(user.Password, pass)==0
                                select user).FirstOrDefault();
                                  
                return userSeek==null ? "User not found":userSeek.ToString();
            }
        }

        //2. Вывод данных о всех счетах заданного пользователя
        public static IEnumerable<Account> GetAllAccountsByUser(int userId)
        {
            using (AppContext db = new AppContext())
            {
                var accountsSeek = (from account in db.Accounts
                                    where account.UserId == userId
                                    select account).ToList();
                return accountsSeek;
            }
        }


        //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public static void GetAllAccountsHistoryByUser(int idUser)
        {
            using (AppContext db = new AppContext())
            {
                var accountsSeek = (from account in db.Accounts
                                    join history in db.Histories on account.Id equals history.AccountId
                                    where account.UserId == idUser
                                    select new { accountId = account.Id, historyId = history.Id, operationTime = history.OperationTime, sum = history.Sum });                                   
                foreach (var item in accountsSeek)
                {
                    Console.WriteLine("AccountNumber: "+item.accountId+", "+" History: "+item.historyId+" Operation time: "+item.operationTime+", Sum: "+item.sum);
                }
            }
        }

        //4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        public static void GetAllCreditOperationsWithAccountMark()
        {
            using (AppContext db = new AppContext())
            {                
                var accountsSeek = (from account in db.Accounts
                                    join history in db.Histories on account.Id equals history.AccountId
                                    where history.OperationType==OperationType.Credit
                                    select new { userId = account.UserId, operationTime = history.OperationTime, sum = history.Sum });
                foreach (var item in accountsSeek)
                {
                    Console.WriteLine("UserId: " + item.userId + ", " + " Operation time: " + item.operationTime + ", Sum: " + item.sum);
                }
            }
        }
        //5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся извне и может быть любой)
        public static void GetUserWhereDebitMoreX(decimal X)
        {
            using (AppContext db = new AppContext())
            {
                var accountsSeek = from account in db.Accounts
                                    where account.Debit >= X
                                    select account;
                foreach (var item in accountsSeek)
                {
                    Console.WriteLine(item.ToString());
                }
            }
        }
        public static void SeedDataBase()
        {
            using (AppContext context = new AppContext())
            {
                context.Users.Add(new User() { Name = "Ivan", Surname = "Ivanov", Patronimic = "Ivanovich", Phone = "333-35-95", PassportData = "984567854", RegistrationDate = DateTime.Parse("2017-01-01"), Login = "1111", Password = "1111" });
                context.Users.Add(new User() { Name = "Petr", Surname = "Petrov", Patronimic = "Petrovich", Phone = "654-84-52", PassportData = "542215698", RegistrationDate = DateTime.Parse("2017-02-02"), Login = "2222", Password = "2222" });
                context.Users.Add(new User() { Name = "Aleksandr", Surname = "Aleksandrov", Patronimic = "Aleksandrovich", Phone = "347-38-34", PassportData = "54485366", RegistrationDate = DateTime.Parse("2017-03-03"), Login = "3333", Password = "3333" });
                context.SaveChanges();
            }
            using (AppContext context = new AppContext()) 
            {
                context.Accounts.Add(new Account() { AccountOpenDate = DateTime.Parse("2018-01-01"), Debit = 45_000, UserId = 0 });
                context.Accounts.Add(new Account() { AccountOpenDate = DateTime.Parse("2018-02-02"), Debit = 55_000, UserId = 0 });
                context.Accounts.Add(new Account() { AccountOpenDate = DateTime.Parse("2018-02-02"), Debit = 30_000, UserId = 1 });                
                context.SaveChanges();
            }

            using (AppContext context = new AppContext())
            {
                context.Histories.Add(new History() { OperationTime = DateTime.Parse("2018-05-05"), OperationType = OperationType.Credit, Sum = 400, AccountId = 0 });
                context.Histories.Add(new History() { OperationTime = DateTime.Parse("2018-06-06"), OperationType = OperationType.Credit, Sum = 800, AccountId = 1 });
                context.Histories.Add(new History() { OperationTime = DateTime.Parse("2018-07-07"), OperationType = OperationType.Credit, Sum = 300, AccountId = 2 });
                context.SaveChanges();
            }
        }


    }
}
