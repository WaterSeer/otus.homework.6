﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_LINQ
{
    class User
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string  Name { get; set; }
        public string  Surname { get; set; }
        public string  Patronimic { get; set; }
        public string Phone { get; set; }
        public string PassportData { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return this.Surname + " " + this.Patronimic + " " + this.Name + " Passport: " + this.PassportData; 
        }
    }

    
}
