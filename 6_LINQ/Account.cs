﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_LINQ
{
    class Account
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime AccountOpenDate { get; set; }
        public decimal Debit { get; set; }
        public int UserId { get; set; }

        public override string ToString()
        {
            return "AccountId: " + this.Id + " , Debit: " + this.Debit; 
        }

    }
}
